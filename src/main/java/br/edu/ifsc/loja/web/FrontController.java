/**
 * Adatapado do Controller 
 * @author sergions
 * 
 */
package br.edu.ifsc.loja.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import br.edu.ifsc.loja.controller.Controller;
import br.edu.ifsc.loja.model.Usuario;

@WebServlet(urlPatterns="/manager")
public class FrontController extends HttpServlet {

	private static final long serialVersionUID = -2450870764104682549L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String controller = req.getParameter("controller");
		String action = req.getParameter("action");
		String pagina = "/index.jsp";
		Usuario usuario =  null; 

		if ( ObjectUtils.allNotNull(controller)) {
			usuario = (Usuario) req.getSession().getAttribute("usuarioLogado");
			if (StringUtils.isEmpty(action)){
				action = new String();
			}
		
			if ( !exigeLogin(controller, action) ||
				(exigeLogin(controller, action) && ObjectUtils.allNotNull(usuario) )
				){
				try {
					Controller instancia = getController(controller);
					pagina = instancia.executa(req, resp);
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException e) {
					throw new ServletException(e);
				}
			}
		} 
		RequestDispatcher dispatcher = req.getRequestDispatcher(pagina);
		dispatcher.forward(req, resp);
	}
	
	private Controller getController(String controller) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
		controller = StringUtils.capitalize(controller);
		controller = "br.edu.ifsc.loja.controller." + controller +"Controller";
		Class<?> tipo = Class.forName(controller);
		Controller instancia = (Controller) tipo.newInstance();
		return instancia;
	}
	
	private boolean exigeLogin(String controller, String action){
		return !(  controller.equals("login") ||
				  (controller.equals("produto") && action.equals("lista")) ||
				  (controller.equals("usuario") && action.equals("novo"))
				  );
	}
	
	
	
	
}
