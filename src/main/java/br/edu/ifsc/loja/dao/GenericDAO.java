/*
 * @author  Sérgio Nicolau da Silva <sergions@gmail.com>
 * Adaptado do DAO.java do exercício Livraria
 */
package br.edu.ifsc.loja.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.edu.ifsc.loja.model.BaseEntity;

public abstract class GenericDAO<M extends BaseEntity> implements InterfaceDAO<M> {
	
	protected final Map<Long, M> MODELS;
	
	public GenericDAO(Class<M> classe) {
		this.MODELS = new HashMap<>();
	}
	
	protected boolean geraIdEAdiciona(M entity) {
		long id = MODELS.size();
		entity.setId(id);
		return (MODELS.put(id, entity) != null);
	}
	
	@Override
	public boolean insert(M entity) {
		return geraIdEAdiciona(entity);
	}

	@Override
	public boolean delete(M entity) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public M findById(Long id) {
		return MODELS.get(id);
	}
	
	@Override
	public List<M> all() {
		return new ArrayList<M>(MODELS.values());
	}
	
	
	@Override
	public boolean update(M entity) {
		return (MODELS.put(entity.getId(), entity) != null);
		
	}

}
