package br.edu.ifsc.loja.dao;

import java.util.Iterator;

import br.edu.ifsc.loja.model.Usuario;

public class UsuarioDAO extends GenericDAO<Usuario> {
	private static UsuarioDAO instancia;

	public static synchronized UsuarioDAO getInstance() {
		if (instancia == null) {
			instancia = new UsuarioDAO();
		}
		return instancia;
	}

	public UsuarioDAO() {
		super(Usuario.class);
		this.populateUsuario();
	}

	public Usuario buscaPorEmailESenha(String login, String senha) {
		Iterator<Usuario> it = this.MODELS.values().iterator();
		Usuario usuarioLogado = null;
		Usuario usuario = null;
		while (it.hasNext()){
			usuario = it.next();
			if (usuario.getLogin().equals(login) &&
				usuario.getSenha().equals(senha)){
				usuarioLogado = usuario;
				break;
			}
		}
		return usuarioLogado;
	}
	
	private void populateUsuario(){
		Usuario usuario = new Usuario();
		usuario.setNome("Sérgio Nicolau da Silva");
		usuario.setCpf("871.547.309-00");
		usuario.setLogin("sergio");
		usuario.setSenha("123");
		System.out.println(usuario);
		this.insert(usuario);
	}

}