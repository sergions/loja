package br.edu.ifsc.loja.dao;

import java.util.Iterator;

import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.Produto;
import br.edu.ifsc.loja.model.Usuario;

public class CarrinhoDAO extends GenericDAO<Carrinho> {
	private static CarrinhoDAO instancia;

	public static synchronized CarrinhoDAO getInstance() {
		if (instancia == null) {
			instancia = new CarrinhoDAO();
		}
		return instancia;
	}

	public CarrinhoDAO() {
		super(Carrinho.class);
	}

	public Carrinho getCarrinhoUsuario(Usuario usuario) {
		Iterator<Carrinho> it = this.MODELS.values().iterator();
		Carrinho carrinho;
		Carrinho carrinhoUsuario = null;
		while (it.hasNext()){
			carrinho = it.next();
			if (carrinho.getUsuario().equals(usuario)){
				carrinhoUsuario = carrinho;
				break;
			}
		}
		if (carrinhoUsuario == null){
			carrinhoUsuario = new Carrinho(usuario);
			this.insert(carrinhoUsuario);
		}
		return carrinhoUsuario;
		
	}
}
