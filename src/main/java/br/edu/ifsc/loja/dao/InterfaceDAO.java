package br.edu.ifsc.loja.dao;

import java.util.List;

import br.edu.ifsc.loja.model.BaseEntity;

public interface InterfaceDAO<M extends BaseEntity> {
	public boolean insert(M entity);
	public boolean delete(M entity);
	public List<M> all();
	public M findById(Long id);
	public boolean update(M entity);
}
