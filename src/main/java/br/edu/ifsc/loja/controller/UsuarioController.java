package br.edu.ifsc.loja.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import br.edu.ifsc.loja.dao.UsuarioDAO;
import br.edu.ifsc.loja.model.Usuario;

public class UsuarioController implements Controller {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse response) {
		String action = req.getParameter("action");
		String pagina ="/index.jsp";
		if (!StringUtils.isEmpty(action)){
			switch (action) {
			case "lista":
				  pagina = lista(req);
				break;
			case "novo":
				  pagina = novo(req);
				break;
			}
		}
		return pagina;
		
	}

	private String novo(HttpServletRequest req) {
		Usuario usuario = new Usuario();
		usuario.setNome(req.getParameter("nome"));
		usuario.setCpf(req.getParameter("cpf"));
		usuario.setLogin(req.getParameter("login"));
		usuario.setSenha(req.getParameter("senha"));
		
		UsuarioDAO.getInstance().insert(usuario);
		
		return "/index.jsp";
	}

	private String lista(HttpServletRequest req){
		List<Usuario> usuarios = UsuarioDAO.getInstance().all();
		req.setAttribute("usuarios", usuarios);
		return "/WEB-INF/jsp/usuario/usuario.jsp";
	}
}
