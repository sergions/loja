package br.edu.ifsc.loja.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import br.edu.ifsc.loja.dao.UsuarioDAO;
import br.edu.ifsc.loja.model.Usuario;


public class LoginController implements Controller {

//	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
//		String pagina = "/index.jsp";
//		try {
//			String login = req.getParameter("login");
//			String senha = req.getParameter("senha");
//			Usuario usuario = UsuarioDAO.getInstance().buscaPorEmailESenha(login, senha);
//			HttpSession session = req.getSession(true);
//			if (usuario != null) {
//				session.setAttribute("usuarioLogado", usuario);
//				pagina = "/manager?controller=carrinho&action=novo";
//			} else {
//				session.setAttribute("usuarioLogado", null);
//				req.setAttribute("erro", "email ou senha inválidos.");
//			}
//		} catch (Throwable theException) {
//			System.out.println(theException);
//		}
//		RequestDispatcher requestDispatcher = req
//				.getRequestDispatcher(pagina);
//		requestDispatcher.forward(req, resp);
//	}

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse response) {
		String action = req.getParameter("action");
		String pagina ="/index.jsp";
		if (!StringUtils.isEmpty(action)){
			switch (action) {
			case "login":
				  pagina = login(req);
				break;
			case "logout":
				  pagina = logout(req);
				break;
			}
		}
		return pagina;
	}

	private String logout(HttpServletRequest req) {
		HttpSession session = req.getSession(true);
			session.setAttribute("usuarioLogado", null);
			session.setAttribute("carrinho", null);
		return "/index.jsp";
	}

	private String login(HttpServletRequest req) {
		String pagina = "/index.jsp";
		try {
			String login = req.getParameter("login");
			String senha = req.getParameter("senha");
			Usuario usuario = UsuarioDAO.getInstance().buscaPorEmailESenha(login, senha);
			HttpSession session = req.getSession(true);
			if (usuario != null) {
				session.setAttribute("usuarioLogado", usuario);
				pagina = "/manager?controller=carrinho&action=novo";
			} else {
				session.setAttribute("usuarioLogado", null);
				session.setAttribute("carrinho", null);
			}
		} catch (Throwable theException) {
			System.out.println(theException);
		}
		return pagina;
	}

}
