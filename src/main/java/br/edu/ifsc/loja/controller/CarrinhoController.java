package br.edu.ifsc.loja.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.edu.ifsc.loja.dao.CarrinhoDAO;
import br.edu.ifsc.loja.dao.ProdutoDAO;
import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.ItemCompra;
import br.edu.ifsc.loja.model.Produto;
import br.edu.ifsc.loja.model.Usuario;

public class CarrinhoController implements Controller {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse response) {
		String action = req.getParameter("action");
		if (action == null) {
			action = "exibe";
		}
		String pagina = "/index.jsp";
		if (action != null) {
			switch (action) {
			case "adiciona":
				pagina = this.adiciona(req);
				break;
			case "exibe":
				pagina = this.exibe(req);
				break;
			case "remove":
				pagina = this.remover(req);
				break;
			case "novo":
				pagina = this.novo(req);
				break;
			case "atualiza":
				pagina = this.atualiza(req);
				break;
			}

		}
		return pagina;
	}

	private String atualiza(HttpServletRequest req) {
		HttpSession sessao = req.getSession();
		
		Carrinho carrinho = (Carrinho) sessao.getAttribute("carrinho");
		String[] quantidades = req.getParameterValues("quantidade");
		String[] idsProduto = req.getParameterValues("idProduto");
		
		ItemCompra itemCompra = null;
		Long quantidade = null;
		Long idProduto = null;
		
		for (int i = 0; i < idsProduto.length; i++) {
			quantidade = Long.parseLong(quantidades[i]);
			idProduto = Long.parseLong(idsProduto[i]);
			
			itemCompra = carrinho.getItemByProduto(idProduto);
			
			if (itemCompra.getQuantidade().compareTo(quantidade) != 0) {
				carrinho.updateItemCompra(itemCompra, quantidade);
			}
		}
		return "/index.jsp";
	}

	private String exibe(HttpServletRequest req) {

		return "/WEB-INF/jsp/carrinho/carrinho.jsp";
	}

	private String remover(HttpServletRequest req) {
		Carrinho carrinho = (Carrinho) req.getSession().getAttribute("carrinho");
		Long idProduto = Long.parseUnsignedLong(req.getParameter("idProduto"));
		carrinho.removeItem(idProduto);

		return "/WEB-INF/jsp/carrinho/carrinho.jsp";
	}

	private String adiciona(HttpServletRequest req) {
		Carrinho carrinho = (Carrinho) req.getSession(false).getAttribute("carrinho");
		
		if (carrinho == null) {
			
			Usuario usuario = (Usuario) req.getAttribute("usuarioLogado");
			carrinho = CarrinhoDAO.getInstance().getCarrinhoUsuario(usuario);
		
			req.getSession(false).setAttribute("carrinho", carrinho);
		
		}
		
		Long idProduto = Long.parseUnsignedLong(req.getParameter("idProduto"));
		ItemCompra itemCompra = carrinho.getItemByProduto(idProduto);
		Long quantidade = Long.parseLong(req.getParameter("quantidade"));

		if (itemCompra == null) {
		
			Produto produto = ProdutoDAO.getInstance().findById(new Long(idProduto));
			itemCompra = new ItemCompra(produto, quantidade);
			carrinho.addItemCompra(itemCompra);
			
		} else {
			
			carrinho.updateItemCompra(itemCompra, quantidade);
			
		}

		return "/index.jsp";
	}

	private String novo(HttpServletRequest req) {
		
		HttpSession sessao = req.getSession();
		Usuario usuario = (Usuario) sessao.getAttribute("usuarioLogado");
		CarrinhoDAO dao = CarrinhoDAO.getInstance();
		Carrinho carrinho = dao.getCarrinhoUsuario(usuario);
		if (carrinho == null){
			carrinho = new Carrinho(usuario);
			dao.insert(carrinho);
		} 
		
		sessao.setAttribute("carrinho", carrinho);
		
		return "/index.jsp";
	}
}
