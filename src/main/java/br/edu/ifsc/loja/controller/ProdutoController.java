package br.edu.ifsc.loja.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifsc.loja.dao.ProdutoDAO;
import br.edu.ifsc.loja.model.Produto;

public class ProdutoController implements Controller {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse response) {
		String action = req.getParameter("action");
		String pagina = "/index.jsp";
		if (action != null){
			switch (action) {
			case "lista":
				pagina = this.lista(req);
				break;
			}
		}
		return pagina;
	}

	private String lista(HttpServletRequest req){
		List<Produto> produtos = ProdutoDAO.getInstance().all();
		req.setAttribute("produtos", produtos);
		return "/WEB-INF/jsp/produto/lista.jsp";
	}
}
