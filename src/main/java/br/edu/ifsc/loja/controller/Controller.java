package br.edu.ifsc.loja.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Controller {
	String executa(HttpServletRequest req, HttpServletResponse response);
}
