package br.edu.ifsc.loja.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Carrinho extends BaseEntity{
	
	private Long id; 
	
	private Usuario usuario;
	
	private Map<Long, ItemCompra> itens;
	
	private BigDecimal total;

	public BigDecimal getTotal() {
		return total;
	}

	public Carrinho(Usuario usuario){
		this.usuario = usuario;
		this.itens = new HashMap<Long, ItemCompra>();
		this.total = new BigDecimal(0);
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public Collection<ItemCompra> getItens(){
		return this.itens.values();
	}
	
	public void addItemCompra(ItemCompra itemCompra){
		this.itens.put(itemCompra.getProduto().getId(), itemCompra);
		this.total = this.total.add(itemCompra.getProduto().getValor().multiply(new BigDecimal(itemCompra.getQuantidade())));
	}
	
	public ItemCompra getItemByProduto(Long idProduto){
		return this.itens.get(idProduto);
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void updateItemCompra(ItemCompra itemCompra, Long quantidade) {
		itemCompra.setQuantidade(itemCompra.getQuantidade() + quantidade);
		this.total = this.total.add(itemCompra.getProduto().getValor().multiply(new BigDecimal(quantidade)));
		
	}

	public void removeItem(Long idProduto) {
		ItemCompra item = this.itens.remove(idProduto);
		this.total = this.total.subtract(item.getProduto().getValor().multiply(new BigDecimal(item.getQuantidade())));
	}
	
	
}
