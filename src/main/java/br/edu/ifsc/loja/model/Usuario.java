package br.edu.ifsc.loja.model;

import java.util.Date;

public class Usuario extends BaseEntity {
	
	private Long id;
	
	private String login;
	
	private String senha;
	
	private String nome;
	
	private String cpf;
	
	private Date dataCadastro;
	
	public Usuario(){
		this.cpf = "";
		this.senha = "";
		this.dataCadastro = new Date();
		this.id = -1L;
		this.login = "";
		this.nome = "";
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	 @Override
	public String toString() {
		 return this.nome;
	}
	 
	public boolean equals(Usuario usuario) {
		return usuario.id.equals(this.id);
	}
	
	public Date getDataCastro(){
		return this.dataCadastro;
	}

}
