package br.edu.ifsc.loja.model;

import java.math.BigDecimal;

public class Produto extends BaseEntity{

	private Long id;
	
	private Integer codigo;
	
	private String nome;
	
	private String descricao;
	
	private BigDecimal valor;
	
	private String imagem;
	
	public Produto(){
		this(new Integer(0),"","",new BigDecimal(0.0),"");
	}
	
	public Produto(Integer codigo, String nome, String descricao, BigDecimal valor, String imagem){
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
		this.imagem = imagem;
		this.id = codigo.longValue();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

}
