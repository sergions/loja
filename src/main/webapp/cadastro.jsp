<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

	<div class="container">
		<h1><f:message key="register" /></h1>
		<form class="form" method="POST" action="/loja/manager?controller=usuario&action=novo">
			<div class="form-group">
				<label for="nome"><f:message key="name" /></label> 
				<input type="text" class="form-control" name="nome"
					placeholder="<f:message key="name" />">
			</div>
			<div class="form-group">
				<label for="cpf"><f:message key="social_security_number" /></label> 
				<input type="text" class="form-control" name="cpf"
					placeholder="<f:message key="ssc_format"/>">
			</div>
			<div class="form-group">
				<label for="login"><f:message key="login" /></label> 
				<input type="text" class="form-control" name="login"
					placeholder="seu.usuario">
			</div>
			<div class="form-group">
				<label for="senha"><f:message key="password" /></label> 
				<input type="password" class="form-control" name="senha"
					placeholder="senha">
			</div>
			<button type="submit" class="btn btn-primary"><f:message key="registre" /></button>
		</form>
	</div>

	<c:import url="/WEB-INF/jsp/footer.jsp" />