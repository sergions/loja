<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="id" required="true"%>
<%@ attribute name="codigo" required="true"%>
<%@ attribute name="nome" required="true"%>
<%@ attribute name="descricao" required="true"%>
<%@ attribute name="imagem" required="true"%>
<%@ attribute name="valor" required="true"%>

<div class="media">
	<div class="media-left media-middle">
		<a href="#"> <img class="media-object" width="200px"
			src="${imagem}" alt="${nome}">
		</a>
	</div>
	<div class="media-body">
		<h4 class="media-heading">${fn:toUpperCase(nome)}</h4>
		${descricao}<br />
		<h3 class="preco">
			<span class="label label-default"> <strong><f:message key="price" />: </strong>&nbsp;
				<f:formatNumber value="${valor}" type="currency" />
			</span> &nbsp;
			<a class="btn btn-primary btn-sm" href="/loja/manager?controller=carrinho&action=adiciona&quantidade=1&idProduto=${id}" 
			   role="button"><f:message key="add_to_cart" /></a>
		</h3>
	</div>
</div>
