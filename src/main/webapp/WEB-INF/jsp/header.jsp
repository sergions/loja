<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><f:message key="application" /></title>

<c:import url="/WEB-INF/jsp/css.jsp" />
<c:import url="/WEB-INF/jsp/js.jsp" />

</head>
	<c:import url="/WEB-INF/jsp/navbar.jsp" />