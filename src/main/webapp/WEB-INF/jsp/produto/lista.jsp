<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="loja" tagdir="/WEB-INF/tags" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

	<div class="container">

	<c:forEach items="${produtos}" var="produto">
		<loja:produto id="${produto.id}" descricao="${produto.descricao}"  valor="${produto.valor}" nome="${produto.nome}"  imagem="${produto.imagem}" codigo="${produto.codigo}"/>
		
	</c:forEach>
	</div>
	<c:import url="/WEB-INF/jsp/footer.jsp" />
