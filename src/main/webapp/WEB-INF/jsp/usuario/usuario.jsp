<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="loja" tagdir="/WEB-INF/tags" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<strong><f:message key="name"/></strong>
			</div>
			<div class="col-md-3">
				<strong><f:message key="social_security_number"/></strong>
			</div>
			<div class="col-md-3">
				<strong><f:message key="login"/></strong>
			</div>
			<div class="col-md-3">
				<strong><f:message key="date_of_registration"/></strong>
			</div>
		</div>
		<c:forEach items="${usuarios}" var="usuario">
			<div class="row">
				<div class="col-md-3">${usuario.nome}</div>
				<div class="col-md-3">${usuario.cpf}</div>
				<div class="col-md-3">${usuario.login}</div>
				<div class="col-md-3">
					<f:formatDate pattern="dd/MM/yyyy hh:mm"
						value="${usuario.dataCastro}" />
				</div>
			</div>
		</c:forEach>
	</div>
	<c:import url="/WEB-INF/jsp/footer.jsp" />
