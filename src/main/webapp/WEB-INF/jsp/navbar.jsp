<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="/loja/"><f:message key="application" /></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<c:if test="${empty usuarioLogado }">
					<form class="navbar-form navbar-right" method="POST"
						action="/loja/manager?controller=login&action=login">
						<div class="form-group">
							<input type="text" name="login" placeholder="<f:message key="login"/>"
								class="form-control">
						</div>
						<div class="form-group">
							<input type="password" placeholder="<f:message key="password"/>" name="senha"
								class="form-control">
						</div>
						<button type="submit" class="btn btn-success"><f:message key="login"/></button>
					<a href="/loja/cadastro.jsp" class="btn btn-default"><f:message key="register"/></a>
					</form>
				</c:if>
				<c:if test="${not empty usuarioLogado }">
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a title="<f:message key="users" />" class="dropdown-toggle" href="/loja/manager?controller=usuario&action=lista"><span class="glyphicon glyphicon glyphicon-user blue">&nbsp;<f:message key="users" /></span></a></li>
							<li>
								<a href="/loja/manager?controller=carrinho"><span class="carrinho glyphicon glyphicon-shopping-cart">&nbsp;&nbsp;<f:formatNumber type="currency" value="${carrinho.total}" /> </span></a>
							</li>
							<li><a title="<f:message key="logout" />"  class="dropdown-toggle" href="/loja/manager?controller=login&action=logout"><span class="glyphicon glyphicon-log-out blue"></span>&nbsp;<f:message key="logout" /></a></li>
						</ul>
					</div>
				</c:if>
			</div>
		</div>
	</nav>
