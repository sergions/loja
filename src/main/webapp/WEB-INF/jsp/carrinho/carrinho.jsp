<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="loja" tagdir="/WEB-INF/tags" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

	<div class="container">
		<div class="carrinho">
			<form method="post"
				action="/loja/manager?controller=carrinho&action=atualiza" class="form">
				<div class="row">
					<div class="col-md-5"><strong><f:message key="name"/></strong></div>
					<div class="col-md-2"><strong><f:message key="unit_price"/></strong></div>
					<div class="col-md-2"><strong><f:message key="amount"/></strong></div>
					<div class="col-md-1"><strong><f:message key="delete"/></strong></div>
					<div class="col-md-2"><strong><f:message key="total"/></strong></div>
				</div>
				<c:forEach items="${carrinho.itens}" var="item">
					<div class="row">
						<div class="col-md-5">${item.produto.nome}</div>
						<div class="col-md-2">
							<f:formatNumber type="currency" currencySymbol="R$"
								value="${item.produto.valor}" />
						</div>
						<div class="col-md-2">
							<input type="hidden" value="${item.produto.id}" name="idProduto"/>
							<input type="text" class="form-control" name="quantidade"
								value="${item.quantidade}" />
						</div>
						<div class="col-md-1">
							<a href="/loja/manager?controller=carrinho&action=remove&idProduto=${item.produto.id}" >
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</div>
						<div class="col-md-2">
							<f:formatNumber type="currency" currencySymbol="R$"
								value="${item.produto.valor * item.quantidade}" />
								
						</div>
					</div>
				</c:forEach>
				<div class="row totalizador">
					<div class="col-md-8"></div>
					<div class="col-md-2">
						<input class="btn btn-primary btn-xs" type="submit" value="<f:message key="update"/>" />
					</div>
					<div class="col-md-2">
						<strong> <f:formatNumber type="currency"
								currencySymbol="R$" value="${carrinho.total}" />
						</strong>
					</div>
				</div>
			</form>
		</div>
	</div>
	<c:import url="/WEB-INF/jsp/footer.jsp" />
